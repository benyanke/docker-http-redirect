# Docker Redirect Tool

Minimal docker image for doing a 301 or 302 HTTP redirect.

## Description

This image can be used to provide a 301 or 302 redirect to a static location provided at runtime.

## Configuration

The following environment variables are used to configure the container.

 - `REDIRECT_URL` - the URL you wish to redirect to. This is the only required option.

 - `REDIRECT_TYPE` - either `301` or `302`, for the response type you wish to run. Default to `301` (permanent redirect).


## Container Images

The following images are build by CI:

On tags for releases:
 - `registry.gitlab.com/benyanke/docker-http-redirect:latest`
 - `registry.gitlab.com/benyanke/docker-http-redirect:{tag}`

On every push to branches:
 - `registry.gitlab.com/benyanke/docker-http-redirect:branch_{branchname}`
 - `registry.gitlab.com/benyanke/docker-http-redirect:commit_{commit_hash}`

Note that the `branch_*` and `commit_*` tags are cleaned up regularly.

## Release Process

Test the `commit_*` image after pushing the commit, and do local testing. If it works, make a git tag
matching the pattern `vX.X.X` where `X` is an integer.
