package main

import (
	"github.com/gorilla/handlers"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
)

// List of valid redirect options
var validResponseCodes []int = []int{301, 302}

var responseCode int
var defaultResponseCode int = 301
var redirectUrl string

var port int = 8080

func main() {

	// ---------------------------
	// Config and validation
	// ---------------------------

	err := false

	redirectUrl = os.Getenv("REDIRECT_URL")

	responseCodeRaw := os.Getenv("REDIRECT_TYPE")

	// Handle default case
	if len(responseCodeRaw) == 0 {
		responseCode = defaultResponseCode
	} else {
		responseCode, _ = strconv.Atoi(responseCodeRaw)
	}

	// Validate input code
	if !intInSlice(responseCode, validResponseCodes) {
		log.Error("Invalid redirect type, please set $REDIRECT_TYPE to 301 or 302")
		err = true
	}

	if redirectUrl == "" {
		log.Error("$REDIRECT_URL can not be blank")
		err = true
	}

	if err {
		log.Fatal("Can not continue due to validation errors")
	}

	// ---------------------------
	// Validation complete, now start server
	// ---------------------------

	//w := logger.Writer()
	//defer w.Close()

	log.WithFields(log.Fields{
		"port":         port,
		"redirectUrl":  redirectUrl,
		"responseCode": responseCode,
	}).Info("Starting redirect app")

	http.HandleFunc("/", redirect)

	// Fix logging for requests to use logrus
	httpErr := http.ListenAndServe(":"+strconv.Itoa(port), handlers.LoggingHandler(os.Stdout, http.DefaultServeMux))
	if httpErr != nil {
		log.Fatal("ListenAndServe: ", httpErr)
	}

}

func redirect(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Webserver", "gitlab.com/benyanke/docker-http-redirect")

	http.Redirect(w, r, redirectUrl, responseCode)

}

// TODO : test this
func intInSlice(a int, list []int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
