module gitlab.com/benyanke/docker-http-redirect

go 1.13

require (
	github.com/gorilla/handlers v1.5.1
	github.com/sirupsen/logrus v1.8.1
)
