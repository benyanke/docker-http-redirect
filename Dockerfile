##################
# Build and Test stage
##################
FROM golang:1.23 AS build_base

RUN apt-get update && \
    apt-get install -y \
      git \
    && rm -rf /var/cache/apt/*

# Set the Current Working Directory inside the container
WORKDIR /tmp/app

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

# Get deps and clean up unneeded ones
RUN go mod download

# Copy in app
COPY . .

# Formatting Check - fail if code is not run through go fmt
# Human readable output for easier triage. Then fail build if diff exists
RUN test -z $(gofmt -l /tmp/app)

# Unit tests
RUN CGO_ENABLED=0 go test -v

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./out/app .

##################
# Final Stage
##################
FROM scratch

EXPOSE 8080

# Copy our static executable.
COPY --from=build_base /tmp/app/out/app /usr/bin/app

CMD ["/usr/bin/app"]
